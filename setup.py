#!/usr/bin/env python3
from os import path
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README'), encoding='utf-8') as f:
    long_description = f.read()

def _install_reqs():
    with open('requirements.txt') as f:
        return f.read().split('\n')

setup(
    name='poorer',
    version='0.3.dev0',
    author="Cyril Roelandt",
    author_email="tipecaml@gmail.com",
    url="https://git.framasoft.org/Steap/poorer",
    description='Simple expense management system',
    long_description=long_description,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    install_requires = _install_reqs(),
    # Include data specified in MANIFEST.in
    include_package_data = True,
    packages = find_packages(),
    entry_points = {
        'console_scripts': ['poorer = poorer.server:main'],
    },
    zip_safe=False,
)
