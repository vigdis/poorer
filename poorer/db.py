# Copyright (c) 2017, Cyril Roelandt
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from contextlib import contextmanager
import os

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.sql import func, label, desc
from sqlalchemy.pool import StaticPool
import sqlamp
import yaml
try:
    from yaml import CSafeLoader as Loader
except ImportError:
    from yaml import Loader

# Use an in-memory database and make sure the connection can be shared between
# threads.
# See https://docs.sqlalchemy.org/en/latest/dialects/sqlite.html#using-a-memory-database-in-multiple-threads
engine = create_engine('sqlite://',
                       connect_args={'check_same_thread': False},
                       poolclass=StaticPool)
Base = declarative_base()
metadata = sqlalchemy.MetaData(engine)

BaseNode = declarative_base(metadata=metadata,
                            metaclass=sqlamp.DeclarativeMeta)


class Category(Base, BaseNode):
    __tablename__ = 'categories'
    __mp_manager__ = 'mp'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    parent = relationship('Category', remote_side=id)
    parent_id = Column(Integer, ForeignKey('categories.id'))

    def children(self):
        return self.mp.query_children().all()

    def descendants(self):
        return self.mp.query_descendants(and_self=True).all()

    def __repr__(self):
        return("Category<%s>" % (
                self.name))

    def query_amount(self, session):
        return session.query(
            label('total', func.coalesce(func.sum(Spending.amount), 0))).\
            filter(Category.name.in_([s.name for s in self.descendants()])).\
            join(Spending.category)


class Earning(Base):
    __tablename__ = 'earnings'

    id = Column(Integer, primary_key=True)
    date = Column(Date)
    amount = Column(Integer)
    from_ = Column(String)

    def __repr__(self):
        return ("<Earning(%r %.2f %s)" % (
            self.date, self.amount/100, self.from_))


class Spending(Base):
    __tablename__ = 'spendings'

    id = Column(Integer, primary_key=True)
    date = Column(Date)
    amount = Column(Integer)
    to = Column(String)
    category_id = Column(Integer, ForeignKey('categories.id'))
    category = relationship('Category', foreign_keys=[category_id])

    def __repr__(self):
        return ("<Spending(%r %.2f %s [%s])" % (
            self.date, self.amount/100, self.to, self.category.name))

    @property
    def year(self):
        return self.date.year

    @property
    def month(self):
        return self.date.month


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)


@contextmanager
def get_session():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def yaml_to_db(folder):
    # Read categories
    categories = {}
    root = Category(name='root')
    unknown = Category(name='Unknown', parent=root)
    with get_session() as session:
        session.add(root)
        session.add(unknown)
    categories['root'] = root
    categories['Unknown'] = unknown

    mappings = {}
    with open(os.path.join(folder, 'categories.yaml')) as f:
        d = yaml.load(f, Loader=Loader)

    for category in d.get('categories', []):
        name = category['name']
        parent_name = category.get('parent', 'root')
        parent = categories[parent_name]
        with get_session() as session:
            c = Category(name=name, parent=parent)
            session.add(c)
            categories[name] = c

    # Read the category mappings. These are not stored in the database.
    for mapping in d.get('categories-mappings', []):
        try:
            mappings[mapping['to']] = mapping['category']
        except KeyError:
            print(mapping)

    # Read spendings/earnings
    with get_session() as session:
        for filename in os.listdir(folder):
            if not filename.endswith('.yaml'):
                continue

            filename = os.path.join(folder, filename)
            with open(filename) as f:
                d = yaml.load(f, Loader=Loader)

            for spending in d.get('spendings', []):
                to = spending['to']
                try:
                    category = spending['category']
                except KeyError:
                    try:
                        category = mappings[to]
                    except KeyError:
                        print(to)
                category = categories[category]
                entry = Spending(date=spending['date'],
                                 amount=100*spending['amount'],
                                 to=to, category=category)
                session.add(entry)

            for earning in d.get('earnings', []):
                entry = Earning(date=earning['date'],
                                amount=100*earning['amount'],
                                from_=earning['from'])
                session.add(entry)


def get_spendings_by_recipients(min_time=None, max_time=None):
    with get_session() as session:
        query = session.query(
                    Spending.to,
                    label('total', func.sum(Spending.amount))
                ).group_by(Spending.to).\
                order_by(desc('total'))

        if min_time is not None:
            query = query.filter(min_time <= Spending.date)
        if max_time is not None:
            query = query.filter(Spending.date <= max_time)

    return query.all()


def get_spendings_by_categories(min_time=None, max_time=None):
    with get_session() as session:
        top_category = session.query(Category).\
            filter(Category.name == 'root').one()
        ret = []
        for res in top_category.children():
            query = res.query_amount(session)
            if min_time is not None:
                query = query.filter(min_time <= Spending.date)
            if max_time is not None:
                query = query.filter(Spending.date <= max_time)
            res.total = query.one()[0]
            if res.total > 0:
                ret.append(res)

        session.expunge_all()
        ret.sort(key=lambda x: x.total, reverse=True)
        return ret


def get_spendings_for_recipient(recipient):
    with get_session() as session:
        query = session.query(
                    Spending
                ).filter(Spending.to == recipient)
    return query.all()


def get_recipients():
    with get_session() as session:
        query = session.query(Spending.to).group_by(Spending.to)

    return map(lambda x: x[0], query.all())


def get_earnings_by_source(min_time=None, max_time=None):
    with get_session() as session:
        query = session.query(
                    Earning.from_,
                    label('total', func.sum(Earning.amount))
                ).group_by(Earning.from_).\
                order_by(desc('total'))

        if min_time is not None:
            query = query.filter(min_time <= Earning.date)
        if max_time is not None:
            query = query.filter(Earning.date <= max_time)

    return query.all()


def clear():
    metadata.reflect(bind=engine)
    with get_session() as session:
        for table in reversed(metadata.sorted_tables):
            session.execute(table.delete())
