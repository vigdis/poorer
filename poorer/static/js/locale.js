function locale_currency_symbol() {
    return numeral.localeData(numeral.options.currentLocale).currency.symbol
}

function locale_currency_str(amount, symbol=true) {
    /* Properly format a given amount of money according to the selected
       locale. If SYMBOL is true, adds the currency symbol to the returned
       string. */
    var fmt = '0,0.00'
    var ret = null

    if (symbol) {
        if (locale_currency_symbol() == '$')
            fmt = '$' + fmt
        else
            fmt = fmt + '$'
    }
    return numeral(amount).format(fmt)
}

function locale_percent(percent, symbol=true) {
    /* Properly format a given percentage according to the selected locale.
       If SYMBOL is true, adds the '%' symbol to the returned string. */
    var ret = numeral(percent/100).format('00,00.00%')
    if (!symbol)
        ret = ret.slice(0, -1)
    return ret
}
